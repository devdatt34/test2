import { AuthenticationGuard } from './../authentication.guard';
import { AuthenticationServiceService } from './../../../../Test1/src/app/services/authentication-service.service';
import { from } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router, CanActivate } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loginData = [{ 'email': 'test@test.com', 'pass': '12345678' }];

  error_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'minlength', message: 'Email length.' },
      { type: 'maxlength', message: 'Email length.' },
      { type: 'email', message: 'please enter a valid email address.' }
    ],

    'password': [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'password length.' },
      { type: 'maxlength', message: 'password length.' }
    ]
  }

  constructor(private formBuilder: FormBuilder, private router: Router) {
    this.loginForm = formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30),
        Validators.email
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)
      ]))

    })
  }


  ngOnInit(): void {
  }

  postData() {
    if (this.loginData[0].email === this.loginForm.value.email && this.loginData[0].pass === this.loginForm.value.password) {
      this.router.navigate(['dashboard']);
      alert("Login successsfully");
    } else {
      this.router.navigate(['']);
      alert("Login Failed");
    }


  }
}
